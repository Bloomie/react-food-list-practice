import React, {useState} from 'react';

const ListAddForm = (props) => {
	// console.log(props.message);

	const [food, setFood] = useState({name: null, quantity: 0})

	const handleSubmit = (e) => {
		e.preventDefault();
		props.addFood(food);
	}

	// const handleChangeName = (e) => {
	// 	setFood({
	// 		...food,
	// 		name: e.target.value
	// 	})
	// }

	// const handleChangeQuantity = e => {
	// 	setFood({
	// 		...food,
	// 		quantity: e.target.value
	// 	})
	// }

	const handleChange = e => {
		setFood({
			...food,
			[e.target.id] : e.target.value
		})
	} 

	return (
		<div>
			{props.message}
			<form onSubmit={handleSubmit}>
				<label htmlFor="name" >Add food to buy: </label>
				<input type="text" id="name" name="name" onChange={handleChange} />
				<br/>

				<label htmlFor="quantity">Quantity: </label>
				<input type="number" id="quantity" name="quantity" onChange={handleChange}/>
				
				<br/>
				<button> Submit </button>
			</form>
		</div>
	)
}

export default ListAddForm;
