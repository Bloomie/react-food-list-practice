import React, {useState} from 'react';

const ListUpdateForm = (props) => {

	const [food, setFood] = useState({name: "", quantity: 0})

	const handleChangeSelected = e => {
		setFood(props.foods.find(listedFood => {
			return listedFood.id === e.target.value
		}))
	}

	const handleChange = e => {
		setFood({
			...food,
			[e.target.name] : e.target.value
		})
	}

	const handleSubmit = e => {
		e.preventDefault();
		props.editFood(food)
	}

	let foodList = props.foods.map(food => {
		return <option key={food.id} value={food.id}>{food.name}</option> 
		
	})

		return (
			<div>
				<form onSubmit={handleSubmit}>
					<label htmlFor="id">Select Food to Edit</label>
					<br/>
					<select name="id" id="id" onChange={handleChangeSelected}>
						{foodList}
					</select>
					<br/>

				{/*{name}*/}
				<label>Name:</label>
				<input 
				type="text" 
				name="name" 
				value={food.name} 
				onChange={handleChange}/>
					<br />
				{/*{quantity}*/}
				<label>Quantity:</label>
				<input 
				type="number" 
				name="quantity" 
				value={food.quantity} 
				onChange={handleChange} />
					<br/>
				<button>Edit</button>
			</form>	
			</div>
		);
	}

	export default ListUpdateForm;

