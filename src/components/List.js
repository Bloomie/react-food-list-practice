import React from 'react';

const List = ( props ) => {

	let foodList = props.foods.map( food => {
		return(
			<li key={food.id} >
			<button onClick={() => {props.deleteFood(food.id)}} >&times;</button> 
			{food.name} - {food.quantity}
			</li>
		)
	})

	return (

		<>
		{
			// if there are to food available display this
			props.foods.length === 0 ?
			 "There are no Food to buy" : 
			 	(
					<ul>
						{foodList}
					</ul>

				 )
			}
		</>
	)

}


export default List;
