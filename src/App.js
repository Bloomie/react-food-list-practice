import React, { useState } from 'react';
import ListAddForm from './components/ListAddForm';
import List from './components/List';
import {v1 as uuid} from 'uuid';
import ListUpdateForm from './components/ListUpdateForm'

function App() {

  // state
    // data container
    const [ message, setMessage ] = useState("Hello!")
    
    const [ foods, setFoods] = useState([
      {name: "Karne" , quantity: 7, id: uuid() },
      {name: "Gulay" , quantity: 8, id: uuid() },
      {name: "Sapatos" , quantity: 5, id: uuid() }
    ]);

    const addFood = food => {
      setFoods([
          ...foods,
          {
            name: food.name,
            quantity: food.quantity,
            id: uuid()
          }
        ])
    }


    const deleteFood = id => {
      setFoods(foods.filter(food => food.id !== id))
    }

    const editFood = food => {
      setFoods(foods.map(listedFood => {
         return food.id === listedFood.id ? food : listedFood
      }))
    }


  return (
    <div>
      <ListAddForm message={message} addFood={addFood}/>
      <List foods={foods} deleteFood= {deleteFood}/>

      {/*if there are food display this also*/}
      {
        foods.length ?
        <ListUpdateForm foods = {foods} editFood = {editFood} /> : ""
      }
    </div>
  );
}

export default App;
